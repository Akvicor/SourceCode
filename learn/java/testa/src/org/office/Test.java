package org.office;

public class Test {
    public static void main(String[] args) {
        System.out.print("父类信息测试：");
        Work wo = new Work();
        System.out.println(wo.work());
        System.out.print("测试工作类信息测试：");
        TestWork te = new TestWork("测试工作",10,5);
        System.out.println(te.work());
        System.out.print("研发工作类信息测试：");
        DevelopmentWork de = new DevelopmentWork("研发工作",1000,10);
        System.out.println(de.work());
    }
}
