package org.office;

public class DevelopmentWork extends Work{
    // 属性：有效编码行数、目前没有解决的Bug个数
    private int availableLine;
    private int unBugNumber;
    //编写构造方法，并调用父类相关赋值方法，完成属性赋值
    public DevelopmentWork(){

    }
    public DevelopmentWork(String name,int availableLine, int unBugNumber){
        this.setName(name);
        this.setAvailableLine(availableLine);
        this.setUnBugNumber(unBugNumber);
    }
    // 公有的get***/set***方法完成属性封装

    public int getAvailableLine() {
        return availableLine;
    }

    public void setAvailableLine(int availableLine) {
        this.availableLine = availableLine;
    }

    public int getUnBugNumber() {
        return unBugNumber;
    }

    public void setUnBugNumber(int unBugNumber) {
        this.unBugNumber = unBugNumber;
    }

    // 重写运行方法，描述内容为：**的日报是：今天编写了**行代码，目前仍然有**个bug没有解决。其中**的数据由属性提供
    public String work() {
        return this.getName()+"的日报是：今天编写了"+this.getAvailableLine()+"行代码，目前仍然有"+this.getUnBugNumber()+"个bug没有解决。";
    }
}
