package org.kvxi.test;

import org.kvxi.animal.Animal;
import org.kvxi.animal.Cat;
import org.kvxi.animal.Dog;

public class Test {
    public static void main(String[] args) {
        Cat one = new Cat();
        one.setName("花花");
        one.setSpecies("中华田园猫");
        one.eat();
        one.run();
        System.out.println("==========================");
        Dog two = new Dog();
        two.setName("妞妞");
        two.setMonth(1);
        two.sleep();
        two.eat();
        System.out.println("==========================");
        two.eat("凡凡");
        /*父类无法访问子类的成员,哪怕它是公有的
        Animal three = new Animal();
        three.run();
        three.sleep();
        */
    }
}
