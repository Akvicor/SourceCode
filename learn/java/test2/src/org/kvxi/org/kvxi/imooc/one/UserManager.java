package org.kvxi.org.kvxi.imooc.one;


public class UserManager {
    // 用户信息验证的方法
    public String checkUser(User one, User two) {
        // 判断用户名是否为空，是否一致
        String msg = "";
        String name1 = one.getUserName();
        String name2 = two.getPassword();
        String password1 = one.getPassword();
        String password2 = two.getPassword();
        if(name1==null||name2==null){
            msg = "用户名不能为空";
        }else{
            if(!name1.equals(name2)){
                msg = "用户名不一致";
            }
        }

        // 判断密码是否为空，是否一致
        if(password1==null||password2==null){
            msg = "密码不能为空";
        }else{
            if(!password1.equals(password2)){
                msg = "密码不一致";
            }
        }

        return msg;
    }
}
