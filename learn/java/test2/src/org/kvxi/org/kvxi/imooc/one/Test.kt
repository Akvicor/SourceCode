package org.kvxi.org.kvxi.imooc.one

object Test {
    @JvmStatic
    fun main(args: Array<String>) {
        val u = User("", "123456")
        val u2 = User("Mike", "123456")
        println("用户名:" + u.userName)
        println("密码:" + u.password)
        println("用户名:" + u2.userName)
        println("密码:" + u2.password)
        println("============================")
        val um = UserManager()
        println(um.checkUser(u, u2))
    }
}
