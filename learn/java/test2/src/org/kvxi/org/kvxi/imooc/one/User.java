package org.kvxi.org.kvxi.imooc.one;

public class User {
    //定义属性用户名、密码
    private String userName;
    private String password;

    public User(String userName,String password){
        this.userName = userName;
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
