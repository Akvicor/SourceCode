package org.kvxi.object;

public class Book {
    private String book;
    private String author;
    private String publication;
    private double price;

    public Book(String book,String author,String publication,double price){
        this.book = book;
        this.author = author;
        this.publication = publication;
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        if(price<10){
            System.out.println("图书价格最低10元");
            this.price = 10;
        }else{
            this.price = price;
        }
    }

    public String getAuthor() {
        return author;
    }


    public String getBook() {
        return book;
    }

    public String getPublication() {
        return publication;
    }

    public void setPublication(String publication) {
        this.publication = publication;
    }

    public void introduce(){
        System.out.println("书名："+this.getBook());

        System.out.println("作者："+this.getAuthor());

        System.out.println("出版社："+this.getPublication());

        System.out.println("价格："+this.getPrice());
    }
}
