package org.kvxi.object;

public class CatTest {
    public static void main(String[] args){
        //对象实例化

        Cat one = new Cat();
//        Cat one;//声明对象
//        one = new Cat();//实例化对象

        //测试
        one.eat();
        one.run();
        //类中属性成员的默认值
        System.out.println(one.name);
        System.out.println(one.month);
        System.out.println(one.species);
        System.out.println(one.weight);
    }
}
