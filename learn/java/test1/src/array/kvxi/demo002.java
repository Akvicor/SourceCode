package array.kvxi;
//冒泡排序
public class demo002 {
    public static void main(String[] args) {
        int[] a = {34,53,12,32,56,17};
        System.out.println("排序前");
        for(int b : a)
            System.out.print(b+" ");
        System.out.println();
        int temp;
        for(int i = 0;i<(a.length-1);i++){
            //内层循环控制每次排序
            for(int j= 0;j<a.length-i-1;j++){//每次过后总会按照从大到小的顺序，倒序排好一个
                if(a[j]>a[j+1]){
                    temp = a[j];
                    a[j] = a[j+1];
                    a[j+1] = temp;
                }
            }
        }
        System.out.println("排序后");
        for(int l:a){
            System.out.print(l+" ");
        }
    }


}
