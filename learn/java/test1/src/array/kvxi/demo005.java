package array.kvxi;
import java.util.Scanner;
public class demo005 {
    //求某数X次方值
    private double area(double y , double x){
        double area = 1;
        for(int i = 1; i <= x; i++){
            area *= y;
        }
        return area;
    }
    public static void main(String[] args) {
        demo005 sf = new demo005();
        Scanner sc = new Scanner(System.in);
        System.out.println("输入底数");
        double y = sc.nextDouble();
        System.out.println("输入指数");
        double x = sc.nextDouble();
        double fin = sf.area(y,x);
        System.out.println();
        System.out.println(fin);
    }
}
