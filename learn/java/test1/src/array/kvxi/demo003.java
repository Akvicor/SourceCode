package array.kvxi;
import  java.util.Scanner;
public class demo003 {
    public static void main(String[] args){
        //定义一个三行两列的整型二维数组intArray
        int intArray[][] = new int[3][2];

        //从键盘输入学生成绩，要求输入顺序与效果图一致
        Scanner sc = new Scanner(System.in);
        for(int i = 0; i < intArray.length; i++){
            System.out.println("请输入第"+(i+1)+"个学生的语文成绩：");
            intArray[i][0] = sc.nextInt();
            System.out.println("请输入第"+(i+1)+"个学生的数学成绩：");
            intArray[i][1] = sc.nextInt();
        }
        //求语文的总成绩和平均分
        int chSum = intArray[0][0]+intArray[1][0]+intArray[2][0];
        int chSuma = chSum/3;
        //求数学的总成绩和平均分
        int maSum = intArray[0][1]+intArray[1][1]+intArray[2][1];
        int maSuma = maSum/3;
        System.out.println("语文的总成绩为："+chSum);
        System.out.println("语文的平均分为："+chSuma);
        System.out.println("数学的总成绩为："+maSum);
        System.out.println("数学的平均分为："+maSuma);
    }
}
