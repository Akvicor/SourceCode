package array.kvxi;
import java.util.Scanner;

public class demo001 {
    public static void main(String[] args) {
        System.out.println("请输入元素个数");

        Scanner sc = new Scanner(System.in);

        int count = sc.nextInt();
        int[] a = new int[count];
        //for循环获取每个元素值
        for(int i = 0; i<a.length; i++){
            System.out.println("请输入第"+(i+1)+"个数字");
            a[i] = sc.nextInt();
        }
        System.out.println("数组元素的内容为：");
        //增强型foreach循环输出数组每个元素内容
        for(int element : a){
            System.out.print(element+" ");
        }
        //求和并输出
        System.out.println();
        int sum=0;
        for(int i:a){
            sum += i;
        }
        System.out.println("数组内全部元素和为"+sum);
    }
}
