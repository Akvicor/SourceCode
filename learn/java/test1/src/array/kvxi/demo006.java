package array.kvxi;

/**
 * 方法重载
 */
public class demo006 {
    //求圆形面积
    public double area(double r){
        return r*r*Math.PI;
    }

    //求长方形面积
    public double area(double l,double w){
        return l*w;
    }

    public static void main(String[] args) {
        //定义对象
        demo006 ac = new demo006();
        //定义一个double类型的变量存放半径，并初始化
        double r = 4.5;
        //定义两个变量存放长和宽，并初始化
        double l = 8,w = 5;
        //调用方法，求圆的面积并打印输出
        System.out.println("圆的面积为:"+ac.area(r));
        //调用方法，求长方形面积并打印输出
        System.out.println("长方形的面积为:"+ac.area(l,w));
    }
}
