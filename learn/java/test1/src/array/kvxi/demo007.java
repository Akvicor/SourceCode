package array.kvxi;

/**
 * 可变参数列表
 */

public class demo007 {
    public void sum(int... n){
        int sum = 0;
        for(int i:n){
            sum += i;
        }
        System.out.println("sum = "+sum);
    }
    public static void main(String[] args) {
        demo007 ad = new demo007();
        ad.sum(1);
        ad.sum(1,2,3,4,5);
        //可以将数组传给可变参数列表
        int[] a = {1,2,3,4,5,6,7};
        ad.sum(a);

    }
}
