package org.kvxi;
import java.util.Scanner;
/**
 * 为指定成绩加分
 *
 * Scanner工具类位于java.util.Scanner包中
 * print输出不会换行 println输出会换行
 * 使用断点调试
 */
public class demo002 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);   //创建Scanner对象
        //System.out.println("请输入考试成绩信息：");
        System.out.print("请输入考试成绩信息：");
        int score=input.nextInt();//获取用户成绩保存到变量中
        int count = 0;   //加分次数
    //    int score = 53;   //加分前成绩
        System.out.println("加分前成绩："+score);

        while(score<60){   //加到60分为止
            score++;
            count++;
        }

        System.out.println("加分后成绩："+score);
        System.out.println("共加了"+count+"次！");
    }
}
