package org.kvxi;
import java.util.Scanner;
/**
 * 实现接受三人班级的四名学员的成绩信息，然后计算每个班学员的平均分
 * 二重循环，外层循环控制班级数量，内层循环控制每个班级的学员数量
 */
public class demo003 {
    public static void main(String[] args) {
        int classNum = 3;//班级数量
        int stuNum = 4;//学生数量
        double sum = 0;//分数总和
        double avg = 0;//平均分
        Scanner input = new Scanner(System.in);//crate Scanner objects
        for(int i = 1;i<=classNum;i++){//外层循环控制班级
            sum = 0;
            System.out.println("***Please enter the grade of the "+i+" class***");
            for(int j=1 ; j<=stuNum;j++){//内层循环控制班级内每个学生的成绩
                System.out.println("please enter the score of the "+j+"th student");
                int score = input.nextInt();
                sum= sum+score;
            }
            avg=sum/stuNum;
            System.out.println("第"+i+"个班级学生的平均分为："+avg);
        }


    }
}
