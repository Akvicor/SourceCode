package org.kvxi;

public class demo009 {
    public static void main(String[] args) {
        //char类型和int类型之间的转换
        char c = (char)65536;
        int n;
        n=c;//隐式类型转换
        c=(char)n;

        //整型和浮点型的类型转换
        int x = 100;
        long y = x;
        x=(int)y;
        float f = 1000000000000000l;
        System.out.println("f = "+f);
        float f1 = 132342957023423l;
        System.out.println("f1 = "+f1);
    }
}
