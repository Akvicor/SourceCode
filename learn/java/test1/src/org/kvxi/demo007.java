package org.kvxi;

public class demo007 {
    public static void main(String [] args){
        char s1 = 'A';
        System.out.println("S1 = "+s1);
        char s2 = 6633;
        System.out.println("s2 = "+s2);
        //Unicode编码
        char s3 = '\u1700';
        System.out.println("s3 = "+s3);
        String s4 = "A\u1701\u2710 \u1245A";
        System.out.println("s4 = "+s4);
        //布尔值
        boolean s5 = true;
        System.out.println("s5 = "+s5);
        //换行符
        System.out.println(s1+"||"+s2);
        System.out.println(s1+"||\n"+s2);
        //加法还是字符串拼接？
        int x=3,y=5;
        int t = '\t';
        int n = '\n';
        System.out.print("1== ");//便于区分
        System.out.println("\\t = "+t);// \t的值
        System.out.print("2== ");
        System.out.println("\\n = "+n);// \n的值
        System.out.print("3== ");
        System.out.println(""+ x +'\t'+ y +'\n');//判定为字符串连接
        System.out.print("4== ");
        System.out.println(""+ x +"\t"+ y +"\n");//判定为字符串连接(使用双引号)
        System.out.print("5== ");
        System.out.println(x +'\t'+ y +'\n');//判定为加法
        System.out.print("6== ");
        System.out.println(x +"\t"+ y +"\n");//判定为字符串连接（使用双引号）
    }
}
