package org.kvxi;

public class demo013 {
    public static void main(String args[]) {
        int a = 3,b = 5;
        System.out.println("a<b="+(a<b));
        System.out.println("a>b="+(a>b));

        //商场打折，如果总价格大于100，减20
        double prise1 = 85,prise2 = 55;//存放价格
        double sum = prise1+prise2;
        System.out.println("原价为"+sum);
        if(sum>=100){
            sum -= 20;
            System.out.println("折后价格为"+sum);
        }
    }
}
