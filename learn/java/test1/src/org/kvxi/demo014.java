package org.kvxi;
import java.util.Scanner;

public class demo014 {
    public static void main(String[] args) {
        System.out.println("请输入一个整数");

        //从键盘接收数据
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();

        if(n%2 == 0){
            System.out.println(n+"是个偶数");
        }else{
            System.out.println(n+"是个奇数");
        }

        System.out.println("使用三目运算符");
        String a;
        a = (n%2==0) ?"是个偶数":"是个奇数";
        System.out.println(n+a);
    }
}
