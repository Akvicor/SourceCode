package org.kvxi;
import java.util.Scanner;

public class demo103 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //int number = 7;//目标数字
        int number = (int) (Math.random() * 10);
        int guess = 0;//用户猜的数字
        int i = 0;//记录猜的次数
        char failed;

        do{
            if(i==0) {
                System.out.println("猜一个在0-9之间的数");
            }else{
                if(guess>number){
                    failed = '大';
                }else {
                    failed = '小';
                }
                System.out.println("您才的数字偏"+failed+"，请再试一次");
            }
            guess = sc.nextInt();
            i++;
        }while(number != guess);
        System.out.println("猜对了");
        System.out.println("共猜了"+i+"次");
    }
}
