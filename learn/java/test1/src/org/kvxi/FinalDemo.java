package org.kvxi;

public class FinalDemo {
    public static void main(String[] args){
        final int I = 1;  //定义常量  一般用大写字母
        int i2 = 2;  //定义变量
        System.out.println("赋值前i="+i2);

        // I=1;  //常量不可重新赋值
        i2 = 3;  //变量可重新赋值
        System.out.println(I);
        System.out.println("赋值后i="+i2);


        final double PI = 3.1415926;
        final double MIN_VALUE = 0;

    }
}
