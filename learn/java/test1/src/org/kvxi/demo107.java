package org.kvxi;

public class demo107 {
    public static void main(String[] args) {
        int ge,shi,bai,sum,sumv;
        //使用for循环

        for(int i=200;i<300;i++){
            //取出百位数
            bai = i/100;
            //取出十位数
            shi = (i-bai*100)/10;
            //取出个位数
            ge = (i-bai*100-shi*10);
            //计算三个数字之积
            sumv = bai*shi*ge;
            //计算三个数字之和
            sum = bai+ge+shi;
            //如果积等于42并且和为12，则将满足条件的数输出
            if(sumv==42&&sum==12){
                System.out.println(i);
            }
        }
    }
}
