package org.kvxi;

public class demo011 {
    public static void main(String[] args) {
        int num1 = 10 ,num2 = 5;
        int result = num1 + num2;
        System.out.println(num1+"+"+num2+"="+result);

        //区分两种情况结果
        System.out.println(num1+num2);
        System.out.println(""+num1+num2);//判定为字符串连接

        //分子分母都是整型时，结果为整除后的结果
        System.out.println(13/5);
        System.out.println(13.0/5);//其中一个为浮点型时

        System.out.println(13%5);
        System.out.println(13.5%5);


    }
}
