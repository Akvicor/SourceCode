package org.kvxi;
import java.util.Scanner;

public class demo015 {
    public static void main(String[] args) {
        System.out.println("请输入年份:");
        Scanner sc = new Scanner(System.in);
        int year = sc.nextInt();
        String s = ((year%4 == 0 && year%100 != 0) || year %400 == 0) ? "是闰年":"不是闰年";
        System.out.println(year+s);

    }
}
