package org.kvxi;
import java.util.Scanner;




public class demo004 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("请输入班级数量：");
        int classNum = input.nextInt();
        System.out.print("请输入班级内学生数量：");
        int studentNum = input.nextInt();
        String[][] studentsName = new String[classNum][studentNum];//储存班级数目及学生数目
        String refresh = "1"; //清除流中剩余换行符
        refresh = input.nextLine();
        for (int i = 0; i < classNum; i++) {//循环控制第几个班级
            System.out.println("**第" + (i + 1) + "个班**");
            for (int j = 0; j < studentNum; j++) {//内层循环获取班级内学生姓名
                System.out.println("输入第" + (j + 1) + "个学生名字：");
                studentsName[i][j] = input.nextLine();
            }
        }
        System.out.println("是否打印学生列表 [Y/n]");//确认是否打印列表（默认为打印）
        String enter = input.nextLine();
        if (!(enter.equals("n"))) {
            System.out.println("*******学生列表*******");
            for (int i = 0; i < classNum; i++) {
                System.out.println("***第" + (i + 1) + "个班级***");
                for (int j = 0; j < studentNum; j++) {
                    if (j + 1 == studentNum) {
                        System.out.println(studentsName[i][j]);
                    } else {
                        System.out.print(studentsName[i][j] + "  ");
                    }
                }
            }
            System.out.println("*******打印完毕*******");
        }
    }
}
