package com.office.Test;

import com.office.Department.Department;
import com.office.Position.Position;
import com.office.Staff.Staff;

public class Test {
    public static void main(String[] args) {
        Position pos1 = new Position("经理","001");
        Position pos2 = new Position("助理","002");
        Position pos3 = new Position("职员","003");
        Department dep1 = new Department("人事部","101");
        Department dep2 = new Department("市场部","102");
        Staff sta1 = new Staff("张铭","S001",29,"男", pos1, dep1);
        Staff sta2 = new Staff("李艾爱","S002",21,"女", pos2, dep1);
        Staff sta3 = new Staff("孙超","S004",29,"男", pos3, dep1);
        Staff sta4 = new Staff("张美美","S005",26,"女", pos3, dep2);
        Staff sta5 = new Staff("蓝迪","S006",37,"男", pos1, dep2);
        Staff sta6 = new Staff("米莉","S007",24,"女", pos3, dep2);

        dep1.addStaff(sta1);
        dep1.addStaff(sta2);
        dep1.addStaff(sta3);
        dep2.addStaff(sta4);
        dep2.addStaff(sta5);
        dep2.addStaff(sta6);

        System.out.println(sta1.introduce());
        System.out.println("===========================================================");
        System.out.println(sta2.introduce());
        System.out.println("===========================================================");
        System.out.println(sta3.introduce());
        System.out.println("===========================================================");
        System.out.println(sta4.introduce());
        System.out.println("===========================================================");
        System.out.println(sta5.introduce());
        System.out.println("===========================================================");
        System.out.println(sta6.introduce());
        System.out.println("===========================================================");
        System.out.println(dep1.introduce());
        System.out.println(dep2.introduce());
    }
}
