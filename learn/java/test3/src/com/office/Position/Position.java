package com.office.Position;

public class Position {
    //职务编号 职务名称
    private String positionNo;
    private String positionName;

    //无参构造
    public Position(){

    }

    //带参构造
    public Position(String positionName,String positionNo){
        this.setPositionName(positionName);
        this.setPositionNo(positionNo);
    }

    /**
     *getter setter 方法
     */
    public String getPositionNo() {
        return positionNo;
    }

    public void setPositionNo(String positionNo) {
        this.positionNo = positionNo;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }
    /**
     * getter setter 方法结束
     */
}
