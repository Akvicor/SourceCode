package com.office.Department;

import com.office.Staff.Staff;

public class Department {
    //部门编号 部门名称
    private String departmentNo;
    private String departmentName;
    private int departmentNum;
    private Staff[] staff;

    //无参构造
    public Department(){

    }

    //带参构造
    public Department(String departmentName,String departmentNo){
        this.setDepartmentName(departmentName);
        this.setDepartmentNo(departmentNo);
    }

    public String getDepartmentNo() {
        return departmentNo;
    }

    public int getDepartmentNum() {
        return departmentNum;
    }

    public void setDepartmentNum(int departmentNum) {
        this.departmentNum = departmentNum;
    }

    public Staff[] getStaff() {
        if(this.staff==null){
            this.staff = new Staff[200];
        }
        return staff;
    }

    public void setStaff(Staff[] staff) {
        this.staff = staff;
    }

    /**
     *setter getter 方法
     */
    public void setDepartmentNo(String departmentNo) {
        this.departmentNo = departmentNo;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }
    /**
     * setter getter 方法结束
     */

    public void addStaff(Staff sta){
        for(int i = 0; i < this.getStaff().length; i++){
            if(this.getStaff()[i]==null){
                this.getStaff()[i]=sta;
                this.departmentNum = i+1;
                return;
            }
        }
    }

    public String introduce(){
        return this.departmentName+"总共有"+this.departmentNum+"名员工";
    }
}
