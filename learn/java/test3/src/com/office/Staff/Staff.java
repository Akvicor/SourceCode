package com.office.Staff;
import com.office.Department.Department;
import com.office.Position.*;

public class Staff {
    //员工姓名 工号 年龄 性别 所属部门 职务信息
    private String name;
    private String no;
    private int age;
    private String sex;
    private Position position;
    private Department department;

    public Staff(){

    }

    public Staff(String name,String no, int age, String sex, Position position, Department department){
        this.setName(name);
        this.setNo(no);
        this.setAge(age);
        this.setSex(sex);
        this.setPosition(position);
        this.setDepartment(department);
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if(age>65||age<18){
            this.age = age;
        }else{
            this.age = age;
        }
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        if(sex.equals("男")||sex.equals("女")){
            this.sex = sex;
        }else{
            this.sex = "男";
        }

    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public String introduce(){
        return  "\n姓名:"+this.getName()+
                "\n工号:"+this.getNo()+
                "\n性别:"+this.getSex()+
                "\n年龄:"+this.getAge()+
                "\n职务;"+this.getDepartment().getDepartmentName()+this.getPosition().getPositionName();
    }
}
