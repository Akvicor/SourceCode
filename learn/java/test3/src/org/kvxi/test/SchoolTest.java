package org.kvxi.test;

import org.kvxi.model.Student;
import org.kvxi.model.Subject;


public class SchoolTest {
    public static void main(String[] args) {
        //测试Subject
        Subject sub1 = new Subject("计算机科学与技术","j0001",4);
//        System.out.println(sub1.info());
//        System.out.println("===============================================");
        //测试Student
        Student stu1 = new Student("S01","张三","男",18);
//        System.out.println(stu1.introduce());
//        System.out.println("===============================================");
//        Student stu2 = new Student("S01","张三","男",18);
//        System.out.println(stu2.introduce());
//        System.out.println("===============================================");
//        System.out.println(stu1.introduce(sub1.getSubjectName(),sub1.getSubjectNo(),sub1.getSubjectLife()));
//        System.out.println("===============================================");
//        System.out.println(stu1.introduce(sub1));
        //测试指定专业中有多少学生报名
        sub1.addStudent(stu1);
//        sub1.addStudent(stu2);
        System.out.println("计算机 科学与应用的专业中已有"+sub1.getStudentNum());
    }
}
