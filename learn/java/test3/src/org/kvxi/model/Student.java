package org.kvxi.model;

public class Student {
    //成员属性: 学号 姓名 性别 年龄

    private String studentNo;
    private String studentName;
    private String studentSex;
    private int studentAge;

    private Subject studentSubject;

    //无参构造
    public Student() {
    }

    //多参构造
    public Student(String studentNo, String studentName, String studentSex, int studentAge) {
        this.setStudentNo(studentNo);
        this.setStudentName(studentName);
        this.setStudentSex(studentSex);
        this.setStudentAge(studentAge);
    }
    public Student(String studentNo, String studentName, String studentSex, int studentAge,Subject studentSubject) {
        this.setStudentNo(studentNo);
        this.setStudentName(studentName);
        this.setStudentSex(studentSex);
        this.setStudentAge(studentAge);

        this.setStudentSubject(studentSubject);
    }

    /**
     * 获取专业对象 如果没有 现实里话再返回
     * @return 专业对象信息
     */
    public Subject getStudentSubject() {
        if(this.studentSubject==null)
            this.studentSubject = new Subject();
        return studentSubject;
    }

    public void setStudentSubject(Subject studentSubject) {
        this.studentSubject = studentSubject;
    }

    public String getStudentNo() {
        return studentNo;
    }

    public void setStudentNo(String studentNo) {
        this.studentNo = studentNo;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentSex() {
        return studentSex;
    }

    public void setStudentSex(String studentSex) {
        if(studentSex.equals("男") || studentSex.equals("女")) {
            this.studentSex = studentSex;
        }else{
            this.studentSex = "Null";
        }
    }

    public int getStudentAge() {
        return studentAge;
    }

    public void setStudentAge(int studentAge) {
        if(studentAge<10||studentAge>100){
            this.studentAge = 18;
        }
        this.studentAge = studentAge;
    }

    /**
     * 学生自我介绍的方法
     * @return 自我介绍的信息,包括姓名 学号 性别 年龄
     */
    public String introduce(){
        return "学生信息如下:\n" +
                "姓名" +this.getStudentName()+
                "\n学号:"+this.getStudentNo()+
                "\n性别:"+this.getStudentSex()+
                "\n年龄:"+this.getStudentAge()+
                "\n专业名称:"+this.getStudentSubject().getSubjectName()+
                "\n专业编号:"+this.getStudentSubject().getSubjectNo()+
                "\n专业学年:"+this.getStudentSubject().getSubjectLife();
    }

    /**
     *学生自我介绍的方法
     * @param subjectName 专业名称
     * @param subjectNo 专业编号
     * @param subjectLife 专业学制
     * @return 自我介绍信息 自我介绍的信息,包括姓名 学号 性别 年龄,专业名称 专业编号 专业学年
     */
    public String introduce(String subjectName,String subjectNo,int subjectLife){
        return "学生信息如下:\n" +
                "姓名" +this.getStudentName()+
                "\n学号:"+this.getStudentNo()+
                "\n性别:"+this.getStudentSex()+
                "\n年龄:"+this.getStudentAge()+
                "\n专业名称:"+subjectName+
                "\n专业编号"+subjectNo+
                "\n专业学年"+subjectLife;
    }

    /**
     * 学生自我介绍的方法
     * @param mySubject 所选专业的对象
     * @return 自我介绍信息 自我介绍的信息,包括姓名 学号 性别 年龄,专业名称 专业编号 专业学年
     */
    public String introduce(Subject mySubject){
        return "学生信息如下:\n" +
                "姓名" +this.getStudentName()+
                "\n学号:"+this.getStudentNo()+
                "\n性别:"+this.getStudentSex()+
                "\n年龄:"+this.getStudentAge()+
                "\n专业名称:"+mySubject.getSubjectName()+
                "\n专业编号"+mySubject.getSubjectNo()+
                "\n专业学年"+mySubject.getSubjectLife();
    }


}
